package tpreflect.zetudiants;

import tpreflect.paquetcadeau.PaquetCadeau;

import java.lang.reflect.*;
import java.security.Permission;
import java.util.Arrays;
import java.util.Iterator;
import java.util.Vector;

public class RunMe {
    public static void main(String[] args) throws InvocationTargetException, IllegalAccessException {
        Vector v = new PaquetCadeau().getPaquetCadeau();
        Iterator i = v.iterator();

        boolean invoked = false, altered = false;

        setSecurityManager();

        while (i.hasNext()) {
            Object o = i.next();

            Class reflectioned = o.getClass();
            Method[] methods = reflectioned.getDeclaredMethods();
            Field[] fields = reflectioned.getDeclaredFields();

            System.out.println("----- METHODS -----");
            System.out.println(Arrays.toString(reflectioned.getMethods()));

            System.out.println("----- DECLARED FIELDS -----");
            System.out.println(Arrays.toString(reflectioned.getDeclaredFields()));

            System.out.println("----- FIELDS -----");
            System.out.println(Arrays.toString(reflectioned.getFields()));

            if (methods.length > 0 && !invoked) {
                methods[0].invoke(o);
                invoked = true;
            }

            if (fields.length > 0 && !altered) {
                for(Field field : fields){
                    if(Modifier.isPrivate(field.getModifiers())){
                        field.setAccessible(true);
                        System.out.println(field);
                        field.setDouble(o, 1);
                        System.out.println(field);
                        altered = true;
                    }
                }
            }
        }
    }

    private static void setSecurityManager() {
        System.setSecurityManager(new SecurityManager() {
            @Override
            public void checkPermission(Permission perm) {
                System.out.println(perm);
                if (perm instanceof ReflectPermission && "suppressAccessChecks".equals(perm.getName())) {
                    for (StackTraceElement elem : Thread.currentThread().getStackTrace()) {
                        if ("setAccessible".equals(elem.getMethodName())) {
                            throw new SecurityException();
                        }
                    }
                }
            }
        });
    }
}
