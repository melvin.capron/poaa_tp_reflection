**Question 1**

La méthode `toString()` d'un objet permet de le représenter sous sa forme "textuelle". Par convention, les objets font un `override` de cette méthode.

Cela permet plus précisément de donner une information sur l'objet utilisé.


**Question 2**

Par défaut, la classe `Object` en Java représente un objet de cette façon à travers son `toString` par défaut :

```java
return this.getClass().getName() + "@" + Integer.toHexString(this.hashCode());
```

On y voit ainsi : le nom de la classe de l'objet, suivi de son adresse récupérée de l'hexadecimal.

En utilisant le `toString()` sur la classe du `Vector` on obtient cette représentation : 

```
tpreflect.paquetcadeau.hidden.Type1@38af3868
tpreflect.paquetcadeau.hidden.Type2@77459877
Muahahahahahahaaaa!!!
interface tpreflect.paquetcadeau.hidden.Type4
```


**Question 3** 

Ainsi on en déduit qu'il contient : 

- Une classe nommée `Type1`
- Une classe nommée `Type2`
- Une classe inconnue, ayant rééecrit son `toString()`
- Une interface nommée `Type4`


**Question 4**

Les classes sont considérées comme des objets lorsqu'on les instancie. C'est de cette façon qu'on peut accéder à la reflexivité par exemple.

Pour exemple, il suffit d'instancier une classe et de demander son type. La plupart des langages répondront simplement `Object`.

**Question 5**

On peut accéder à presque toutes les spécifités de la classe, comme : les constructeurs, les attributs, les méthodes et même les annotations.

On peut y accéder facilement à travers ces méthodes : 

```java
Class reflectionClass = o.getClass();
reflectionClass.getConstructors(); // Pour les constructeurs
reflectionClass.getMethods(); // Pour les méthodes
reflectionClass.getFields(); // Pour les attributs
```

**Question 6**

Dans l'exemple suivant, j'appelle la première méthode de la première classe qui en possède au moins une : 

```java
      Class reflectioned = o.getClass();
      Method[] methods = reflectioned.getDeclaredMethods();

      if(methods.length > 0 && !invoked){
          methods[0].invoke(o);
          invoked = true;
      }
```

De cette façon, l'application me répond : 

`moooooo le caribou`

La différence entre `getFields()` et `getDeclaredFields()` est assez importante :

- `getFields()` : retourne tous les attributs publics, ceux de la classe et de la super-classe
- `getDeclaredFields()` : retourne tous les attributs quel que soit la portée, mais uniquement ceux de la classe courante appelée

**Question 7**

L'encapsulation est à mon sens très utile puisqu'elle permet d'éviter tout ce qui est couplage fort en ne gardant les méthodes et/ou attributs qu'aux classes qui le nécessitent : chaque classe est responsable de ce qu'elle fait des données, et quiconque veut interagir avec ces données doit interagir avec cette classe.

Via les attributs `public` et `private` on peut y ajouter une couche de sécurité supplémentaire en évitant de modifier les champs directement. Dans un langage cela pourrait s'apparenter à de la mutation de données sans aucun contrôle.

Ainsi, les `setters` et les `getters` font leur apparition puisque par convention les attributs d'une classe sont en `private` ou `protected` et permettent de contrôler l'intégrité des données de chaque attribut, ce qui permet une plus grande flexbilité et sécurité. 

Le fait de donner une portée aux variables permet aussi de masquer celles-ci à l'utilisateur. La classe devient seule responsable du contrat de modification de données, et elle le contrôle.

**Question 8**

La réflexivité peut être très utilisée dans le mode du développement et encore plus avec toutes les couches d'abstraction qui s'ajoutent en permanence à nos applications.

L'exemple le plus probant qui me vient à l'esprit est sont utilisation par les ORMs en règle général et plus précisément l'ORM `Doctrine` de `Symfony` : 

les entités sont définies et configurées par des annotations, et le moyen de lire ces annotations est de passer par la réflexivité : 

Par exemple : 
```php
/**
 * @Column(type="string", length=32, unique=true, nullable=false)
 */
```

La configuration d'une donnée se fait ainsi très simplement, et le `QueryBuilder` par exemple lors de son insertion de données peut vérifier celle-ci avec les annotations imposées par chaque entité. 

Sachant que celles-ci peuvent être différentes selon l'entité dans laquelle nous travaillons, cela se montre particulièrement intéressant dans un souci de délégation de responsabilités.

Egalement, les tests utilisent aussi la réflexivité pour lire les annotations (JUnit).

**Question 9**

La classe `Class` est en `final` pour éviter que l'on puisse l'étendre. Uniquement la JVM doit s'occuper de la création des classes, et comme le langage Java est très fortement typé, il ne faut pas que l'on puisse modifier la définition d'une classe. 

Plus globalement, `final` doit être utilisé dès que possible pour éviter les abstractions chainées des classes qui font que les classes dépendent plutôt des abstractions potentiellement imbriquées que des interfaces comme cela devrait être le cas.

**Question 10**

Modification d'un des attributs public :

```
            if (fields.length > 0 && !altered) {
                System.out.println(fields[0]);
                fields[0].setInt(o, 1);
                System.out.println(fields[0]);

                altered = true;
            }
```

**Question 11**

D'après la documentation Java et ce que j'ai pu constater, la méthode `setAccessible` permet de forcer ou non le contrôle de la portée de la variable. 

Si la variable est en `private` et que l'on fait un `getAccessible()` cela sera à `false`. 

Si on fait ensuite un `setAccessible(true)` la variable **restera** en `private` mais cette fois-ci elle sera accessible puisqu'on ne vérifiera pas la portée pour la variable donnée.

**Question 12**

En reprenant les conclusions aux questions précédentes je constate : 

**Question 6 :** `getFields()` ne retournera pas cette méthode, même si celle-ci a été au préalable mise en `setAccesible(true)`

**Question 7 :** l'utilisation de portée `private` ne semble pas suffire à contenir toutes les modifications pouvant être opérées depuis l'extérieur

**Question 10 :** La modification de l'attribut en `private` peut se faire en ayant modifié au préalable son accessibilité par `setAccesible(true)`

Pour conclusion, je peux dire qu'il faut des vérifications supplémentaires car la réflexivité permet de s'affranchir du système de portée et donc ouvre des modifications hors du contrat de classes

**Question 13**

Implémentation faite dans le code.
